import React, { Component } from 'react';

import { Switch, Route } from 'react-router-dom';

import Home from '../home/Home';

import Recipes from '../recipe/Recipes';
import Recipe from '../recipe/Recipe';
import RecipeCreateForm from '../recipe/RecipeCreateForm';
import RecipeUpdateForm from '../recipe/RecipeUpdateForm';

import SignIn from '../user/SignIn';
import SignUp from '../user/SignUp';
import Users from '../user/Users';
import User from '../user/User';

export default class Body extends Component {
    render() {
        return(
            <div style={{paddingTop: "100px", paddingBottom: "100px"}}>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/recipes" component={Recipes} />
                    <Route exact path="/recipes/create" component={RecipeCreateForm} />
                    <Route exact path="/recipes/category/:type" component={Recipes} />
                    <Route exact path="/recipes/:id" component={Recipe} />
                    <Route exact path="/recipes/:id/update" component={RecipeUpdateForm} />
                    <Route exact path="/signin" component={SignIn} />
                    <Route exact path="/signup" component={SignUp} />
                    <Route exact path="/users" component={Users} />
                    <Route exact path="/users/:id" component={User} />
                </Switch>
            </div>
        )
    }
}