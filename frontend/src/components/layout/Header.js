import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { axios_instance, getUser } from '../../utils';

export default class Header extends Component {

    constructor(props) {
      super(props);

      this.logout = this.logout.bind(this);
    }

    logout(e) {

      axios_instance.post("/users/signout")
        .then(response => {
          localStorage.removeItem("username");
          localStorage.removeItem("token");
          window.location.href = "/";
        }).catch(err => console.log(err));

        e.preventDefault();
    }

    render() {
        const user = getUser()[0];

        return(
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div className="container">
              <a className="navbar-brand" href="/">Delisioco</a>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link className="nav-link" to="/">Home</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/recipes">Recipes</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/users">Users</Link>
                  </li>
                  {user ? <li className="nav-link">Hello, <Link to={`/users/${user}`}>{user}</Link>&nbsp;&nbsp;<button className="btn btn-primary btn-sm" onClick={(e) => this.logout(e) }>Log Out</button></li> : 
                  <span><li className="nav-item">
                    <Link className="nav-link" to="/signin">Sign In</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/signup">Sign Up</Link>
                  </li>
                  </span>}
                </ul>
              </div>
            </div>
          </nav>
        )
    }
}
