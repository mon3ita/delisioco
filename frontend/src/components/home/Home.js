import React, { Component } from 'react';

import { axios_instance, getUser } from '../../utils';

import { Link } from 'react-router-dom';

import Recipe from '../recipe/Recipe';

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recipes: []
        }

        this.handleDeleteRecipe = this.handleDeleteRecipe.bind(this);
    }

    componentDidMount() {
        axios_instance.get("/recipes")
                    .then(response => {
                        this.setState({
                            recipes: response.data
                        });
                    }).catch(err => console.log(err));
    }

    handleDeleteRecipe(id) {
        const recipes = this.state.recipes.filter(recipe => recipe._id !== id);
        this.setState({ recipes });
    }

    render() {
        const recipes = this.state.recipes.map((recipe, id) =>
            <Recipe recipe={recipe} deleteRecipe={this.handleDeleteRecipe} />
        );

        const user = getUser()[0];

        return(
            <div> 
                {user ? <Link className="btn btn-success btn-sm" to="/recipes/create">Create</Link>
                : ""}
                <hr/>
            {recipes}</div>
        )
    }
}