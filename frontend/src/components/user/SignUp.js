import React, { Component } from 'react';

import { axios_instance } from '../../utils';

export default class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
          username: "",
          password: "",
          errors: []
        }

        this.handleUserCreate = this.handleUserCreate.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    handleUserCreate(e) {

      const errors = [];

      const user = this.state;

      if(!user.username)
        errors.push("Username must be provided.")

      if(!user.password)
        errors.push("Password must be provided.")

      if(user.password.length < 10)
        errors.push("Password must be at least 10 chars long.")

      if(!errors.length) {
        axios_instance.post("/users/signup", user)
          .then(response => {
            window.location.href = "/login";
          }).catch(err => errors.push(err));
      }

      this.setState({ errors });

      e.preventDefault();
    }

    handleUsernameChange(e) {
      this.setState({ username: e.target.value });
    }

    handlePasswordChange(e) {
      this.setState({ password: e.target.value });
    }

    render() {
      const errors = this.state.errors.map((error, id) =>
        <div className="alert alert-danger">{error}</div>
      );

      return(
        <form style={{marginTop: '10%', marginLeft: '10%'}}>
          <h1>Registration</h1>
          <hr/>
          {errors}<br/>
          <div className="form-group">
            <label>Username</label>
            <input type="text" className="form-control" placeholder="Enter username" onChange={(e) => this.handleUsernameChange(e) } />
          </div>
          <div className="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" className="form-control" placeholder="Password" onChange={(e) => this.handlePasswordChange(e) } />
          </div>
          <button className="btn btn-success" onClick={(e) => this.handleUserCreate(e) } >Sign Up</button>
        </form>
      )
    }
}