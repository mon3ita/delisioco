import React, { Component } from 'react';

import { axios_instance } from "../../utils";

import Recipe from '../recipe/Recipe';

export default class User extends Component {
    constructor(props) {
        super(props);

        this.state = {
            recipes: []
        }
    }

    componentDidMount() {

        const id = this.props.match.params.id;

        axios_instance.get(`/users/${id}`)
            .then(response => {
                console.log(response.data);
                this.setState({ recipes: response.data.data });
            }).catch(err => console.log(err));

    }

    render() {
        const recipes = this.state.recipes.map((recipe, id) =>
            <Recipe recipe={recipe} />
        );
        
        return(
            <div>
                {recipes}
            </div>
        )
    }
}