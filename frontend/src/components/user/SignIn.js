import React, { Component } from 'react';

import { axios_instance } from '../../utils';

export default class SignIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
          username: "",
          password: "",
          errors: []
        }

        this.login = this.login.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    handleUsernameChange(e) {
      this.setState({ username: e.target.value });
    }

    handlePasswordChange(e) {
      this.setState({ password: e.target.value });
    }

    login(e) {

      const errors = [];

      const user = this.state;

      if(!user.username)
        errors.push("Username must be provided.")

      if(!user.password)
        errors.push("Password must be provided.")

      if(!errors.length) {
        axios_instance.post("/users/signin", user)
          .then(response => {
            localStorage.setItem("username", response.data.username);
            localStorage.setItem("token", response.data.token);

            window.location.href = "/login";
          }).catch(err => errors.push(err));
      }

      this.setState({ errors });

      e.preventDefault();
    }

    render() {

      const errors = this.state.errors.map((error, id) =>
        <div className="alert alert-danger">{error}</div>
      );

      return(
        <form style={{marginTop: '10%', marginLeft: '10%'}}>
          <h1>Login</h1>
          <hr/>
          {errors}<br/>
          <div className="form-group">
            <label>Username</label>
            <input type="text" className="form-control" placeholder="Enter username" onChange={(e) => this.handleUsernameChange(e)} />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input type="password" className="form-control" placeholder="Password" onChange={(e) => this.handlePasswordChange(e) } />
          </div>
          <button className="btn btn-primary" onClick={(e) => this.login(e) }>Sign In</button>
        </form>
      )
    }
}