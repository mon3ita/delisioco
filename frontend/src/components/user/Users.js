import React, { Component } from 'react';

import { axios_instance } from '../../utils';

import { Link } from 'react-router-dom';

export default class Users extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: []
        }
    }

    componentDidMount() {
        axios_instance.get("/users")
            .then(response => {
                this.setState({users: response.data});
            }).catch(err => console.log(err));
    }

    render() {

        const users = this.state.users.map((user, id) => 
            <tr>
                <td>{id}</td>
                <td><Link to={`/users/${user.username}`}>{user.username}</Link></td>
            </tr>)

        return(
            <div>
                <table className="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Username</th>
                    </tr>
                  </thead>
                  <tbody>
                    {users}
                  </tbody>
                </table>
            </div>
        )
    }
}