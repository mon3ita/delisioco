import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { axios_instance, getUser, getDate, splitIngridients } from '../../utils';

export default class Recipe extends Component {
    constructor(props) {
        super(props);

        this.state = {
            recipe: ""
        }

        this.handleDeleteRecipe = this.handleDeleteRecipe.bind(this);
    }

    componentDidMount() {
        if(!this.props.match) {
            this.setState({recipe: this.props.recipe});
        } else {
            const id = this.props.match.params.id;

            axios_instance.get(`/recipes/${id}`)
                .then(response => this.setState({ recipe: response.data }))
                .catch(err => console.log(err));
        }
    }

    handleDeleteRecipe() {
        const id = this.state.recipe._id;
        const token = getUser()[1];

        axios_instance.delete(`/recipes/${id}/delete`, {
            headers: {
                "Authorization": token
            }
        })
            .then(response => {
                window.location.href = "/recipes";
            }).catch(err => console.log(err));
    }

    render() {
        const recipe = this.state.recipe;
        const user = getUser()[0];

        const showRecipe = <p className="card mb-22">
                <div className="card-body">
                    <h1 className="card-title"><Link to={`/recipes/${recipe._id}`}>{recipe.title}</Link> {user === recipe.author ? <span><Link className="btn btn-primary btn-sm" to={`/recipes/${recipe._id}/update`} >Edit</Link>&nbsp;<button className="btn btn-danger btn-sm" onClick={ this.handleDeleteRecipe }>X</button></span> : ""}</h1>
                    <hr/>
                    <h4>Ingridients: </h4><hr/>
                    <p className="card-text">{recipe.ingridients ? splitIngridients(recipe.ingridients) : ""}</p>
                    <h4>Steps: </h4><hr/>
                    <p className="card-text">{recipe.steps}</p>
                </div>
                <div className="card-footer text-muted">
                    Posted on {getDate(new Date(recipe.created_at))} by&nbsp;
                   {recipe.author} in {recipe.category}
                </div>
            </p>

        return(
            <div className="content">{showRecipe}</div>
        )
    }
}