import React, { Component } from 'react';

import { axios_instance, getUser } from '../../utils';

export default class RecipeUpdateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: "",
            ingridients: "",
            steps: "",
            errors: []
        };

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleIngridientsChange = this.handleIngridientsChange.bind(this);
        this.handleStepsChange = this.handleStepsChange.bind(this);
    }

    componentDidMount() {

        const id = this.props.match.params.id;

        axios_instance.get(`/recipes/${id}`)
                    .then(response => {
                        this.setState({
                            title: response.data.title,
                            ingridients: response.data.ingridients,
                            steps: response.data.steps
                        })
                    }).catch(err => console.log(err));

    }

    handleTitleChange(e) {
        this.setState({ title: e.target.value });
    }

    handleIngridientsChange(e) {
        this.setState({ ingridients: e.target.value });
    }

    handleStepsChange(e) {
        this.setState({ steps: e.target.value });
    }

    handleUpdateRecipe(e) {

        const id = this.props.match.params.id;
        const recipe = {
            title: this.state.title,
            ingridients: this.state.ingridients,
            steps: this.state.steps,
            updated_at: new Date()
        };

        const [user, token] = getUser();

        if(!user)
          window.location.href = "/login";

        const { errors } = this.state;

        if(!recipe.title)
            errors.push("Title must be provided.")
        if(!recipe.ingridients)
            errors.push("Ingridients must be provided.")
        if(!recipe.steps)
            errors.push("Steps must be provided.")

        if(!errors.length) {
            axios_instance.put(`/recipes/${id}/update`, recipe, {
                headers: {
                    "Authorization": token
                }
            }).then(response => {
                window.location.href = `/recipes/${id}`
            }).catch(err => console.log(err));
        }

        this.setState({ errors });
        e.preventDefault();
    }

    render() {
      const errors = this.state.errors.map((error, id) =>
        <div className="alert alert-danger">{error}</div>
      );

      const recipe = this.state;

      return(
            <form>
              <br/>
              {errors}
              <hr/>
              <h1>Update Recipe</h1>
              <hr/>
              <div className="form-group">
                <input type="text" value={recipe.title} className="form-control" placeholder="Title" onChange={(e) => this.handleTitleChange(e)} />
              </div>
              <div className="form-group">
                <textarea className="form-control" value={recipe.ingridients} placeholder="Ingridients" onChange={(e) => this.handleIngridientsChange(e) } ></textarea>
              </div>
              <div className="form-group">
                <textarea className="form-control" value={recipe.steps} placeholder="Steps" onChange={(e) => this.handleStepsChange(e)} ></textarea>
              </div>
              <div className="form-group">
                <select className="form-control" placeholder="Category"></select>
              </div>
              <button type="submit" className="btn btn-primary" onClick={(e) => this.handleUpdateRecipe(e)} >Update</button>&nbsp;
            </form>
      )
    }
}