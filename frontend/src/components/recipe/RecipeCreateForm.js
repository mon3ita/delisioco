import React, { Component } from 'react';

import { axios_instance, getUser } from '../../utils';

export default class RecipeCreateForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
          title: "",
          ingridients: "",
          steps: "",
          category: 0,
          categories: [],
          errors: []
        }

        this.handleCreateRecipe = this.handleCreateRecipe.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleIngridientsChange = this.handleIngridientsChange.bind(this);
        this.handleStepsChange = this.handleStepsChange.bind(this);
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
    }

    componentDidMount() {
      axios_instance.get("/categories")
        .then(response => {
          this.setState({categories: response.data})
        }).catch(err => this.setState({errors: [err.message]}));
    }

    handleCreateRecipe(e) {
      const recipe = {
        title: this.state.title,
        ingridients: this.state.ingridients,
        steps: this.state.steps,
        created_at: new Date(),
        updated_at: new Date(),
        category: this.state.categories[this.state.category].category
      };

      const errors = [];

      const [user, token] = getUser();

      if(!user)
        window.location.href = "/login";

      if(!recipe.title)
        errors.push("Title must be provided.")

      if(!recipe.ingridients)
        errors.push("Ingridients must be provided.")

      if(!recipe.steps)
        errors.push("Steps must be provided.")

      if(!errors.length) {
          axios_instance.post("/recipes/create", recipe, {
            headers: {
              "Authorization": token
            }
          })
          .then(response => {
            if(response.data.error) {
              errors.push(response.data.error);
            } else {
              window.location.href = `/recipes/${response.data.data._id}`;
            }
          }).catch(err => console.log(err));
      } 
        
      this.setState({ errors: errors });
      e.preventDefault();
    }

    handleTitleChange(e) {
      this.setState({
        title: e.target.value
      });
    }

    handleIngridientsChange(e) {
      this.setState({
        ingridients: e.target.value
      });
    }

    handleStepsChange(e) {
      this.setState({
        steps: e.target.value
      });
    }

    handleCategoryChange(e) {
      this.setState({ category: e.target.value });
    }

    render() {
      const errors = this.state.errors.map((error, id) =>
        <div className="alert alert-danger">{error}</div>
      );

      const categories = this.state.categories.map((category, id) =>
        <option value={id}>{category.category}</option>
      );

      return(
            <form>
              <br/>
              {errors}
              <hr/>
              <h1>New Recipe</h1>
              <hr/>
              <div className="form-group">
                <input type="text" className="form-control" placeholder="Title" onChange={(e) => this.handleTitleChange(e)} />
              </div>
              <div className="form-group">
                <textarea className="form-control" placeholder="Ingridients" onChange={(e) => this.handleIngridientsChange(e) } ></textarea>
              </div>
              <div className="form-group">
                <textarea className="form-control" placeholder="Steps" onChange={(e) => this.handleStepsChange(e)} ></textarea>
              </div>
              <div className="form-group">
                <select className="form-control" placeholder="Category" onChange={(e) => this.handleCategoryChange(e) }>
                {categories}
              </select>
              </div>
              <button type="submit" className="btn btn-success" onClick={(e) => this.handleCreateRecipe(e)} >Create</button>&nbsp;
            </form>
      )
    }
}