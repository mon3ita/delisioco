import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { axios_instance, getUser } from '../../utils';

import Recipe from './Recipe';

export default class Recipes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            recipes: [],
            category: ""
        }

        this.handleChangeCategory = this.handleChangeCategory.bind(this);
    }

    componentDidMount() {

        let type = "";
        if(this.props.match.params.type) {
            type += "/category/" + this.props.match.params.type;
        }

        axios_instance.get(`/recipes${type}`)
            .then(response => this.setState({ recipes: response.data }))
            .catch(err => console.log(err));
    }

    handleChangeCategory(type) {
        const category = type;

        this.setState({ category });

        let url = "/recipes";
        if(category)
            url += `/category/${category}`

        axios_instance.get(url)
            .then(response => {
                this.setState({ recipes: response.data });
            }).catch(err => console.log(err));
    }

    render() {
        const recipes = this.state.recipes.map((recipe, id) =>
            <Recipe recipe={recipe} />
        );

        const user = getUser()[0];

        return(
            <div className="content">
                <hr/>
                {user ? <Link className="btn btn-success btn-sm" to="/recipes/create">Create</Link>
                : ""}
                <hr/>
                <h1 className="my-4">Latest Recipes</h1>
                <div className="row">
                    <div className="col-md-8">
                        {recipes}
                    </div>
                    <div className="col">
                        <div className="card my-4">
                            <h5 className="card-header">Categories</h5>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-lg-8">
                                        <ul className="list-unstyled mb-0">
                                            <li>
                                                <Link to="/recipes" onClick={() => this.handleChangeCategory("")}>All</Link>
                                            </li>
                                            <li>
                                                <Link to="/recipes/category/breakfast" onClick={() => this.handleChangeCategory("breakfast")}>Breakfast</Link>
                                            </li>
                                            <li>
                                                <Link to="/recipes/category/lunch" onClick={() => this.handleChangeCategory("lunch")}>Lunch</Link>
                                            </li>
                                            <li>
                                                <Link to="/recipes/category/dinner" onClick={() => this.handleChangeCategory("dinner")}>Dinner</Link>
                                            </li>
                                            <li>
                                                <Link to="/recipes/category/soup" onClick={() => this.handleChangeCategory("soup")}>Soup</Link>
                                            </li>
                                            <li>
                                                <Link to="/recipes/category/salad" onClick={() => this.handleChangeCategory("salad")}>Salads</Link>
                                            </li>
                                            <li>
                                                <Link to="/recipes/category/desert" onClick={() => this.handleChangeCategory("desert")}>Deserts</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}