import axios from 'axios';

const axios_instance = axios.create({
    baseURL: 'http://localhost:5000/api',
    headers: {
        'Content-Type': 'application/json'
    }
});

function getUser() {
    const user = localStorage.getItem("username");
    const token = localStorage.getItem("token");

    return [user, token];
}

function getDate(date) {
    return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
}

function splitIngridients(ingridients) {
    return ingridients.split(",").map((ingridient, it) => <div>{ingridient}</div>);
}

export { axios_instance, getUser, getDate, splitIngridients };