# Delisioco

## How to test it?

1. Clone the repository

2. Enter into the cloned repository

3. Create venv by `python -m venv venv` and activate it

4. Run `python dependencies.py` to install all packages

5. Run `python run.py` to run flask and react

6. Visit `localhost:300n`, where n = 0, 1, 2... which depends from other apps probabily started at the same port

Enjoy :)