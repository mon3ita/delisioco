import os
import threading

def run(name):
    command = ("npm start --prefix ./frontend" if name == "npm" 
                else "cd backend && flask run")
    os.system(command)

threads = []
for name in ["npm", "flask"]:
    threads.append(threading.Thread(target=run, args=(name, ), daemon=True))
    threads[-1].start()

for thread in threads:
    thread.join()