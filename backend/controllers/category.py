from backend import app

from flask import json, jsonify, request
from bson.objectid import ObjectId

from ..service.category import CategoryService

from . import BASE
from . import authorization_required


@app.route(BASE + "/categories", methods=["GET", "OPTIONS"])
def categories():
    return jsonify(CategoryService().find_all())
