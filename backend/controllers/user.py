from backend import app

import jwt
import bcrypt
from flask import json, jsonify, request
from bson.objectid import ObjectId

from . import authorization_required

from ..service.user import UserService

from . import BASE


@app.route(BASE + "/users/signup", methods=["POST"])
def signup():
    user = json.loads(request.data.decode("utf-8"))

    if UserService().find({"username": user["username"]}):
        return {"message": "User with given username already exists."}

    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(user["password"].encode("utf-8"), salt)
    user["password"] = hashed
    created = UserService().create(user)
    return {"message": "Successfully created."}


@app.route(BASE + "/users/signin", methods=["POST"])
def signin():
    user = json.loads(request.get_data(as_text=True))
    exists = UserService().find({"username": user["username"]})
    if exists and bcrypt.checkpw(
        user["password"].encode("utf-8"), exists["password"].encode("utf-8")
    ):
        token = jwt.encode({"id": exists["_id"]}, app.config["SECRET_KEY"])
        username = exists["username"]
        return jsonify({"username": username, "token": token.decode("utf-8")})
    return {"message": "User doesn't exists or password didn't match."}


@app.route(BASE + "/users/signout", methods=["POST"])
@authorization_required
def signout(user):
    request.headers.delete("Authorization")
    return jsonify({"message": "Successfully logged out!"})


@app.route(BASE + "/users")
def all_users():
    return jsonify(UserService().find_all())
