BASE = "/api"

from functools import wraps

import jwt

from bson.objectid import ObjectId
from flask import request

from .. import app
from ..service.user import UserService


def authorization_required(func):
    @wraps(func)
    def decorator(*args, **kwargs):

        token = request.headers.get("Authorization", None)
        if token:
            try:
                data = jwt.decode(token, app.config["SECRET_KEY"])
                user = UserService().find({"_id": ObjectId(data["id"])})
            except:
                return {"message": "Invalid token"}

            return func(user, *args, **kwargs)
        else:
            return {"error": "Token is not valid."}

    return decorator


from . import recipe, user
