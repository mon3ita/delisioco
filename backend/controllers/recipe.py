from backend import app

from flask import json, jsonify, request
from bson.objectid import ObjectId

from ..service.recipe import RecipeService
from ..service.user import UserService

from . import BASE
from . import authorization_required


@app.route(BASE + "/recipes", methods=["GET", "OPTIONS"])
def all_recipes():
    return jsonify(RecipeService().find_all(field="created_at", sort="descending"))


@app.route(BASE + "/recipes/category/<recipe_type>", methods=["GET"])
def get_recipes(recipe_type):
    return jsonify(RecipeService().find_all({"category": recipe_type}))


@app.route(BASE + "/recipes/<recipe_id>", methods=["GET"])
def get_recipe(recipe_id):
    return jsonify(RecipeService().find({"_id": ObjectId(recipe_id)}))


@app.route(BASE + "/recipes/create", methods=["POST"])
@authorization_required
def create_recipe(user):
    recipe = json.loads(request.get_data(as_text=True))
    recipe["author"] = user["username"]
    RecipeService().create(recipe)
    recipe["_id"] = str(recipe["_id"])
    return jsonify({"data": recipe, "message": "Successfully created"})


@app.route(BASE + "/users/<username>", methods=["GET"])
def get_user_recipes(username):
    recipes = RecipeService().find_all(selector={"author": username})
    return jsonify({"data": recipes})


@app.route(BASE + "/recipes/<recipe_id>/update", methods=["PUT"])
@authorization_required
def update_recipe(user, recipe_id):
    recipe = json.loads(request.get_data(as_text=True))
    recipe["author"] = user["username"]
    record = RecipeService().find({"_id": ObjectId(recipe_id)})

    if record["author"] != user["username"]:
        return {"message": "Can not update recipe!"}

    updated = RecipeService().update({"_id": ObjectId(recipe_id)}, recipe)
    if updated:
        return {"message": "Updated Successfully!"}
    return {"message": "Something went wrong."}


@app.route(BASE + "/recipes/<recipe_id>/delete", methods=["DELETE"])
@authorization_required
def delete_recipe(user, recipe_id):
    record = RecipeService().find({"_id": ObjectId(recipe_id)})

    if record["author"] != user["username"]:
        return {"message": "Can not delete recipe!"}

    deleted = RecipeService().delete({"_id": ObjectId(recipe_id)})
    if deleted:
        return {"message": "Deleted Successfully!"}
    return {"message": "Something went wrong."}
