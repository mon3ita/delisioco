from . import MongoDB
from ..models.recipe import RecipeSchema


class RecipeService:
    def __init__(self):
        self.client = MongoDB("recipes")

    def dump(self, data):
        return RecipeSchema().dump(data)

    def find_all(self, selector={}, field="", sort="ascending"):
        recipes = self.client.find_all(selector=selector, field=field, sort=sort)
        return [self.dump(recipe) for recipe in recipes]

    def find(self, data):
        recipe = self.client.find(data)
        return self.dump(recipe)

    def create(self, data):
        recipe = self.client.create(data)
        return self.dump(recipe)

    def update(self, data, recipe, **options):
        affected = self.client.update(data, recipe, **options)
        return affected > 0

    def delete(self, recipe):
        deleted = self.client.delete(recipe)
        return deleted > 0
