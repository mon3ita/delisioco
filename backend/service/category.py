from . import MongoDB
from ..models.category import CategorySchema


def insert_categories():
    client = MongoDB("categories")

    for category in ["breakfast", "lunch", "dinner", "desert", "soup", "salad"]:
        instance = {"category": category}
        client.create(instance)


class CategoryService:
    def __init__(self):
        self.client = MongoDB("categories")

    def dump(self, data):
        return CategorySchema().dump(data)

    def find_all(self, selector={}):
        categories = self.client.find_all(selector)
        if not list(categories):
            insert_categories()

        categories = self.client.find_all(selector)

        return [self.dump(category) for category in categories]
