import os

from pymongo import MongoClient, DESCENDING


class MongoDB:
    def __init__(self, collection):
        mongo_url = os.environ.get("MONGO_URL")
        self.db = MongoClient(mongo_url).delisioco
        self.collection = collection

    def find_all(self, selector={}, field="", sort="ascending"):

        if sort != "ascending":
            return self.db[self.collection].find(selector).sort(field, DESCENDING)

        return self.db[self.collection].find(selector)

    def find(self, selector):
        return self.db[self.collection].find_one(selector)

    def create(self, instance):
        return self.db[self.collection].insert_one(instance)

    def update(self, selector, instance):
        return self.db[self.collection].replace_one(selector, instance).modified_count

    def delete(self, selector):
        return self.db[self.collection].delete_one(selector).deleted_count
