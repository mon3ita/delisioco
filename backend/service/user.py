from . import MongoDB
from ..models.user import UserSchema


class UserService:
    def __init__(self):
        self.client = MongoDB("users")

    def dump(self, data):
        return UserSchema().dump(data)

    def find_all(self, selector={}):
        users = self.client.find_all(selector)
        return [self.dump(user) for user in users]

    def find(self, data):
        user = self.client.find(data)
        return self.dump(user)

    def create(self, data):
        user = self.client.create(data)
        return self.dump(user)

    def update(self, data, user):
        affected = self.client.update(data, user)
        return affected > 0

    def delete(self, user):
        deleted = self.client.delete(user)
        return deleted > 0
