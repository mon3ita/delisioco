from marshmallow import Schema, fields


class CategorySchema(Schema):
    _id = fields.Str()
    category = fields.Str()
