from marshmallow import Schema, fields


class UserSchema(Schema):
    _id = fields.Str()
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    recipes = fields.List(fields.Nested("RecipeSchema", exclude=("author",)))
