from marshmallow import Schema, fields

from .user import UserSchema


class RecipeSchema(Schema):
    _id = fields.Str()
    title = fields.Str()
    ingridients = fields.Str()
    steps = fields.Str()
    category = fields.Str()
    author = fields.Str()
    category = fields.Str()
    created_at = fields.Str()
    updated_at = fields.Str()
